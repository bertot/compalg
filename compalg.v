From mathcomp Require Import all_ssreflect all_algebra all_field.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Declare ML Module "paramcoq".

Import GRing.Theory Num.Theory Num.ExtraDef.

Import Morphisms.

Section common.

Variable Num : Type.
Variables eq le lt : Num -> Num -> bool.
Variable opp : Num -> Num.
Variables add mult : Num -> Num -> Num.
Variable exp : Num -> nat -> Num.
Variable four : Num.
Variable zero : Num.

Local Notation "x == y" := (eq x y) : bool_scope.
Notation "x < y" := (lt x y) : this.
Notation "x <= y" := (le x y) : this.
Notation "x - y" := (add x (opp y)) : this.
Notation "x * y" := (mult x y) : this.
Notation "x + y" := (add x y) : this.
Notation "x ^ n" := (exp x n) : this.

Delimit Scope this with this.
Open Scope this.
(* In this file we explore the possibility to perform computations
   in a rcfType under the assumption that inputs actually are rational
   numbers (or integers), we know that the most complex comparison that
   will occur is a comparison between two numbers of the form
   (a + sqrt b) where a and b are rational numbers.  In that case,
   we want to rely solely on comparison (between rational numbers), subtraction
   addition, and multiplication.

   The ultimate goal is to refine an algorithm based on a real closed field
   into an algorithm based on rational numbers.
*)

(* The following function compares a and b + sqrt c for equality.
   It assumes that c is non-negative *)

Definition abs_rat_quad_eq (a b c : Num) : bool :=
  if c == zero then a == b else if a < b then false else (a - b) ^ 2 == c.

(* The following function compares s + sqrt b and sqrt d for equality.
   It assumes that s, b, and d are non-negative. *)
Definition abs_pre_quad_eq (s b d : Num) : bool :=
  if s ^ 2 <= (b + d) then (s ^ 2 - (b + d)) ^ 2 == four * b * d else false.

(* The following function compare a + sqrt b and c + sqrt d for equality.
   It assumes that b and d are non-negative *)
Definition abs_quad_eq (a b c d : Num) : bool :=
  if b == zero then abs_rat_quad_eq a c d
  else if d == zero then abs_rat_quad_eq c a b
  else if a == c then b == d
  else if a <= c then 
    if d <= b then abs_pre_quad_eq (c - a) b d else false
  else if b <= d then abs_pre_quad_eq (a - c) d b else false.

(* The point is now to provide a function for strict comparison. *)

(* This function computes a < b + sqrt c.
   It assumes that c is non-negative. *)
Definition abs_rat_quad_lt (a b c : Num) : bool :=
  if c == zero then a < b else (a < b) || ((a - b) ^ 2 < c).

(* This function computes s < sqrt d - sqrt b.
   It assumes that s, b, and d are non-negative. *)
Definition abs_pre_quad_lt (s b d : Num) : bool :=
  ((b + d) < s ^ 2) || ((s ^ 2 - (b + d)) ^ 2 < four * b * d).

(* This function computes a + sqrt b < c + sqrt d.
   It assumes that c and d are non-negative. *)
Definition abs_quad_lt (a b c d : Num) : bool :=
  if b == zero then abs_rat_quad_lt a c d
  else if d == zero then ~~abs_rat_quad_lt c a b && ~~abs_rat_quad_eq c a b
  else if a == c then b < d
  else if a <= c then (b <= d) || abs_pre_quad_lt (c - a) b d
  else
    ~~ ((d <= b) || abs_pre_quad_lt (a - c) d b
    || abs_pre_quad_eq (a - c) d b).

Close Scope this.
End common.

Open Scope ring_scope.

Section ab1.

Variable R : rcfType.

(* In this file we explore the possibility to perform computations
   in a rcfType under the assumption that inputs actually are rational
   numbers (or integers), we know that the most complex comparison that
   will occur is a comparison between two numbers of the form
   (a + sqrt b) where a and b are rational numbers.  In that case,
   we want to rely solely on comparison (between rational numbers), subtraction
   addition, and multiplication.

   The ultimate goal is to refine an algorithm based on a real closed field
   into an algorithm based on rational numbers.
*)

Definition rat_quad_eq : R -> R -> R -> bool :=
  abs_rat_quad_eq (Equality.op [eqMixin of R]) <%R -%R +%R (@GRing.exp _) 0%R.

Definition pre_quad_eq : R -> R -> R -> bool :=
  abs_pre_quad_eq (Equality.op [eqMixin of R]) <=%R -%R +%R *%R (@GRing.exp _)
      4%:R.

Definition quad_eq : R -> R -> R -> R -> bool :=
  abs_quad_eq (Equality.op [eqMixin of R]) <=%R <%R -%R +%R *%R (@GRing.exp _)
       4%:R 0%R.

Definition rat_quad_lt :=
  abs_rat_quad_lt (Equality.op [eqMixin of R]) <%R -%R +%R (@GRing.exp _) 0%R.

Definition pre_quad_lt : R -> R -> R -> bool :=
  abs_pre_quad_lt <%R -%R +%R *%R (@GRing.exp _) 4%:R.

Definition quad_lt : R -> R -> R -> R -> bool :=
  abs_quad_lt (Equality.op [eqMixin of R]) <=%R <%R -%R +%R *%R
    (@GRing.exp _) 4%:R 0%R.

(* This lemma makes explicit the comment given for rat_quad_eq *)
Lemma rat_quad_eqP a b c : 0 <= c -> rat_quad_eq a b c = (a == b + sqrtr c).
Proof.
move=> c0; rewrite /rat_quad_eq /abs_rat_quad_eq /=.
rewrite -![Equality.op _ _ _]/(_ == _).
have [/eqP c0' | cn0] := boolP (c == 0).
  by rewrite c0' ler0_sqrtr ?addr0.
have [altb | blea] := boolP (a < b).
  have : a < b + sqrtr c.
    by rewrite (ltr_le_trans altb) // ler_addl sqrtr_ge0.
  by rewrite ltr_neqAle => /andP [] /negbTE ->.
rewrite (addrC b) -subr_eq.
rewrite -[X in _ == X]sqr_sqrtr // exprnP.
rewrite eqr_expz2 //.
  by rewrite subr_ge0 lerNgt.
by apply: sqrtr_ge0.
Qed.

(* This lemma makes explicit the comment given for pre_quad_eq *)

Lemma pre_quad_eqP a b c d : 0 < b -> 0 <= d -> a <= c -> d <= b ->
  pre_quad_eq (c - a) b d = (a + sqrtr b == c + sqrtr d).
Proof.
move => b0 d0 ac db; rewrite /pre_quad_eq /abs_pre_quad_eq.
rewrite -![Equality.op _ _ _]/(_ == _).
case: ifP.
  move=> cmp.
  rewrite (_ : 4 %:R * b * d = (sqrtr b * sqrtr d *+ 2) ^+ 2); last first.
    rewrite -(mulr_natr _ 2) exprnP expfzMl -mulrA mulrC; congr (_ * _).
      by rewrite expfzMl -!exprnP !sqr_sqrtr // ler_eqVlt b0 orbT.
    by rewrite -exprnP exprS expr1 -natrM.
  rewrite -sqrrN exprnP.
  have sqr_expand : b + d - sqrtr b * sqrtr d *+ 2 = (sqrtr b - sqrtr d) ^+ 2.
    rewrite sqrrB !sqr_sqrtr //; last by rewrite ler_eqVlt b0 orbT.
    by rewrite addrAC.
  rewrite eqr_expz2 //.
      rewrite opprB subr_eq (addrC (_ *+ _)) -subr_eq.
      rewrite sqr_expand exprnP eqr_expz2 //.
          by rewrite subr_eq addrAC eq_sym subr_eq eq_sym -(addrC a).
        by rewrite subr_ge0 ler_sqrt.
      by rewrite subr_ge0.
    by rewrite opprB subr_ge0.
  by rewrite -mulr_natr !mulr_ge0 ?sqrtr_ge0.
move/negbT; rewrite -ltrNge => cmp; symmetry; apply/negbTE.
rewrite eq_sym (addrC a) -subr_eq addrAC eq_sym -subr_eq.
have twon0 : 2 != 0 :> int by [].
rewrite -(eqr_expz2 twon0); last by rewrite subr_ge0.
  rewrite -exprnP sqrrB !sqr_sqrtr //; last by rewrite ler_eqVlt b0 orbT.
  rewrite addrAC.
  have : b + d - sqrtr b * sqrtr d *+ 2 < (c - a) ^ 2.  
    apply: ler_lt_trans cmp; rewrite ler_subl_addr ler_addl -mulr_natr.
    by rewrite !mulr_ge0 ?sqrtr_ge0.
  by rewrite ltr_neqAle => /andP[] ->.
by rewrite subr_ge0 ler_sqrt.
Qed.

(* This lemma makes explicit the comment given for quad_eq *)

Lemma quad_eqP a b c d : 0 <= b -> 0 <= d ->
  quad_eq a b c d = (a + sqrtr b == c + sqrtr d).
Proof.
move=> b0 d0; rewrite /quad_eq /abs_quad_eq.
rewrite -![Equality.op _ _ _]/(_ == _).
rewrite -![abs_rat_quad_eq _ _ _ _ _ _ _ _ _]/(rat_quad_eq _ _ _).
rewrite -![abs_pre_quad_eq _ _ _ _ _ _ _ _ _ _]/(pre_quad_eq _ _ _).
case: ifP => [/eqP -> | ]; first by rewrite sqrtr0 addr0 rat_quad_eqP.
case: ifP => [/eqP -> | ]; first by rewrite sqrtr0 addr0 rat_quad_eqP 1?eq_sym.
move=>/negP/negP dn0 /negP/negP bn0.
case: ifP => [/eqP -> | /negP/negP anc].
  by rewrite (inj_eq (addrI c)) eqr_sqrt.
have [lt0b lt0d] : 0 < b /\ 0 < d.
  by rewrite !ltr_neqAle b0 d0 !andbT !(eq_sym 0).
case: ifP => [a_le_c | /negP/negP].
  case: ifP => [d_le_b | /negP/negP]; first by rewrite pre_quad_eqP //. 
  rewrite -ltrNge => b_lt_d.
  suff : a + sqrtr b < c + sqrtr d by rewrite ltr_neqAle => /andP[] /negbTE ->.
  by rewrite ler_lt_add // ltr_sqrt.
rewrite -ltrNge => c_lt_a.
case: ifP => [b_le_d |/negP/negP].
  by rewrite pre_quad_eqP // ler_eqVlt c_lt_a orbT.
rewrite -ltrNge => d_lt_b.
suff : c + sqrtr d < a + sqrtr b.
  by rewrite ltr_neqAle eq_sym => /andP[] /negbTE ->.
by rewrite ltr_add // ltr_sqrt.
Qed.

(* This lemma documents the behavior of  rat_quad_lt *)
Lemma rat_quad_ltP a b c : 0 <= c -> rat_quad_lt a b c = (a < b + sqrtr c).
Proof.
move => c0; rewrite /rat_quad_lt /abs_rat_quad_lt.
case: ifP => [/eqP -> | /negP/negP cn0]; first by rewrite ler0_sqrtr ?addr0.
have [a_lt_b /= | ] := boolP (a < b).
  by symmetry; rewrite (ltr_le_trans a_lt_b) // ler_addl sqrtr_ge0.
rewrite orFb -lerNgt -ltr_subl_addl  => b_le_a.
rewrite -[X in _ < X]sqr_sqrtr //.
by apply: ltr_sqr; rewrite /Num.nneg qualifE ?subr_ge0 // sqrtr_ge0.
Qed.

(* This lemma expands the square of difference of square roots. *)
Lemma sqrtB2 (x y : R) : 0 <= x -> 0 <= y ->
  (sqrtr x - sqrtr y) ^+ 2 = x + y - sqrtr x * sqrtr y *+ 2.
Proof.
by move => x0 y0; rewrite sqrrB !sqr_sqrtr // addrAC.
Qed.

(* This lemma documents the behavior of pre_quad_lt. *)
Lemma pre_quad_ltP a b c d : 0 < b -> 0 <= d -> a <= c -> d <= b ->
  pre_quad_lt (c - a) b d = (a + sqrtr b < c + sqrtr d).
Proof.
move => b0 d0 ac db; rewrite /pre_quad_lt /abs_pre_quad_lt.
have sqr_expand : b + d - sqrtr b * sqrtr d *+ 2 = (sqrtr b - sqrtr d) ^+ 2.
  by rewrite -sqrtB2 // ler_eqVlt b0 orbT.
have /andP [nnegca nnegds] : (c - a \is Num.nneg) &&
                               (sqrtr b - sqrtr d \is Num.nneg).
  by rewrite !qualifE !subr_ge0 ac ler_sqrt.
have [cmp | ] := boolP (b + d < (c - a) ^ 2); last first.
  rewrite -lerNgt => cmp.
  rewrite (_ : 4%:R * b * d = (sqrtr b * sqrtr d *+ 2) ^+ 2); last first.
    rewrite -(mulr_natr _ 2) exprnP expfzMl -mulrA (mulrC 4%:R); congr (_ * _).
      by rewrite expfzMl -!exprnP !sqr_sqrtr // ler_eqVlt b0 orbT.
    by rewrite -exprnP exprS expr1 -natrM.
  rewrite /= -sqrrN opprB.
  set A := (X in X ^+2 < _); set B := (X in _ < X ^+ 2).
  have /andP [Ann Bnn] : (A \is Num.nneg) && (B \is Num.nneg).
    rewrite !qualifE /A /B subr_ge0.
    by rewrite mulrn_wge0 ?andTb ?mulr_ge0 ?sqrtr_ge0 ?andbT.
  have -> : (A ^+ 2 < B ^+ 2) = (A < B) by apply: ltr_sqr.
  rewrite /A /B ltr_subl_addl -ltr_subl_addr sqr_expand.
  suff -> : ((sqrtr b - sqrtr d) ^+ 2 < (c - a) ^+ 2) = 
             (sqrtr b - sqrtr d < c - a).
    by rewrite ltr_subl_addr addrAC ltr_subr_addr addrC.
  by apply: ltr_sqr.
rewrite orTb; symmetry.
rewrite addrC -ltr_subr_addr addrAC -ltr_subl_addr.
rewrite -ltr_sqr // -sqr_expand ltr_subl_addr (ltr_le_trans cmp) ?exprnP //.
by rewrite ler_addl mulrn_wge0 ?mulr_ge0 ?sqrtr_ge0.
Qed.

(* This lemma documents the behavior of quad_lt. *)
Lemma quad_ltP a b c d : 0 <= b -> 0 <= d ->
  quad_lt a b c d = (a + sqrtr b < c + sqrtr d).
Proof.
move=> b0 d0; rewrite /quad_lt /abs_quad_lt.
rewrite -![Equality.op _ _ _]/(_ == _).
rewrite -![abs_rat_quad_lt _ _ _ _ _ _ _ _ _]/(rat_quad_lt _ _ _).
rewrite -![abs_rat_quad_eq _ _ _ _ _ _ _ _ _]/(rat_quad_eq _ _ _).
rewrite -![abs_pre_quad_lt _ _ _ _ _ _ _ _ _]/(pre_quad_lt _ _ _).
rewrite -![abs_pre_quad_eq _ _ _ _ _ _ _ _ _ _]/(pre_quad_eq _ _ _).
case: ifP => [/eqP -> | ]; first by rewrite rat_quad_ltP // sqrtr0 addr0.
move /negP/negP=> bn0.
case: ifP => [/eqP -> | ].
  rewrite rat_quad_ltP // rat_quad_eqP // -lerNgt sqrtr0 addr0 eq_sym.
  by rewrite ltr_neqAle andbC.
move/negP/negP => dn0.
case: ifP => [/eqP -> | ].
  by rewrite ltr_add2l ltr_sqrt // ltr_neqAle eq_sym dn0.
move/negP/negP => anc.
case: ifP => [ac | ].
  have [bd | ] := boolP (b <= d).
    symmetry.
    by rewrite orTb ltr_le_add // ?ler_sqrt ?ltr_neqAle ?(eq_sym 0) ?anc ?dn0.
  rewrite -ltrNge orFb => db; rewrite pre_quad_ltP //.
    by rewrite ltr_neqAle eq_sym bn0.
  by rewrite ler_eqVlt db orbT.
move/negP/negP; rewrite -ltrNge => ca.
have /andP [/andP [lt0b lt0d] leca] : (0 < b) && (0 < d) && (c <= a).
  by rewrite !ltr_neqAle b0 d0 !(eq_sym 0) bn0 dn0 ler_eqVlt ca orbT.
have [bd | ] := boolP (b < d).
  have bd' : b <= d by rewrite ler_eqVlt bd orbT.
  rewrite pre_quad_ltP // pre_quad_eqP //  lerNgt bd orFb orbC -ler_eqVlt.
  by rewrite -ltrNge.
rewrite -lerNgt => db.
rewrite db !orTb; symmetry; apply/negP/negP; rewrite -lerNgt.
by rewrite ler_add // ler_sqrt.
Qed.

(* this function is used as a reference.  It will be later used in lemma to
   show how this function can be refined into a function that relies only
   a rational numbers and their operations. *)
Definition sample (l : list (R * R)) :=
   sort (fun p q => (p.1 + sqrtr p.2) <= (q.1 + sqrtr q.2)) l.

End  ab1.

(* The following functions are the same as in ab1, except that they use
   only rational number operations. *)

Definition rat_q_eq : rat -> rat -> rat -> bool :=
  abs_rat_quad_eq (Equality.op [eqMixin of rat]) <%R -%R +%R (@GRing.exp _)
  0%R.

Definition pre_q_eq :=
  abs_pre_quad_eq (Equality.op [eqMixin of rat]) <=%R -%R +%R *%R
  (@GRing.exp _) 4%:R.

Definition q_eq :=
  abs_quad_eq (Equality.op [eqMixin of rat]) <=%R <%R -%R +%R *%R
  (@GRing.exp _) 4%:R 0%R.

Definition rat_q_lt :=
  abs_rat_quad_lt (Equality.op [eqMixin of rat]) <%R -%R +%R 
  (@GRing.exp _) 0%R.

Definition pre_q_lt : rat -> rat -> rat -> bool :=
  abs_pre_quad_lt <%R -%R +%R *%R (@GRing.exp _) 4%:R.

Definition q_lt :=
  abs_quad_lt (Equality.op [eqMixin of rat]) <=%R <%R -%R +%R *%R
  (@GRing.exp _) 4%:R 0%R.

(* This is the function that refines sample : it has the same behavior
  but it is designed to work on rational numbers.  Please note that
  sample relies on plain comparison between real closed field numbers,
  while this function relies on the restricted comparisons defined by
  q_eq and q_lt. *)
Definition sample_rat (l : list (rat * rat)) :=
   sort (fun p q => q_lt p.1 p.2 q.1 q.2 || q_eq p.1 p.2 q.1 q.2) l.

(* We need to add this function where the code from the sorting function
   in mathematical components is duplicated, because there was a mistake
   made in the design of mathematical components: for the algorithm definition
   it is not necessary to live in an eqType *)

Section sort.

Variable (T : Type) (leT : rel T).

Fixpoint merge s1 :=
  if s1 is x1 :: s1' then
    let fix merge_s1 s2 :=
      if s2 is x2 :: s2' then
        if leT x2 x1 then x2 :: merge_s1 s2' else x1 :: merge s1' s2
      else s1 in
    merge_s1
  else id.

Fixpoint merge_sort_push (s1 : seq T) (ss : seq (seq T)) :=
  match ss with
  | [::] :: ss' | [::] as ss' => s1 :: ss'
  | s2 :: ss' => [::] :: merge_sort_push (merge s1 s2) ss'
  end.

Fixpoint merge_sort_pop s1 ss :=
  if ss is s2 :: ss' then merge_sort_pop (merge s1 s2) ss' else s1.

Fixpoint merge_sort_rec ss s :=
  if s is [:: x1, x2 & s'] then
    let s1 := if leT x1 x2 then [:: x1; x2] else [:: x2; x1] in
    merge_sort_rec (merge_sort_push s1 ss) s'
  else merge_sort_pop s ss.

Definition sort := merge_sort_rec [::].

End sort.

Parametricity list.
Parametricity prod.
Parametricity bool.
Parametricity sort.
Proof.
- by move=> T leT merge s1 x1 s1' merge_s1 [ | x2 s2].
- by move=> T leT merge [ | x1 s1].
- by move=> T leT merge s1 x1 s1' merege_s1 [ | x2 s2].
- by move=> T leT merge [ | x1 s1].
- by move=> T leT merge s1 x1 s1' merege_s1 [ | x2 s2].
- by move=> T leT merge [ | x1 s1].
- by move=> T leT merge_sort_push s1 [ | xx ss].
- by move=> T leT merge_sort_rec ss [ | x s].
Parametricity Done.
Parametricity nat.
Parametricity abs_rat_quad_eq.
Parametricity abs_pre_quad_eq.
Parametricity abs_quad_eq.
Parametricity abs_rat_quad_lt.
Parametricity abs_pre_quad_lt.
Parametricity abs_quad_lt.
Parametricity orb.

Lemma list_R_map {R : rcfType} (l : list (rat * rat)) :
  list_R (fun x y => ((ratr x.1, ratr x.2) : R * R) = y) l
    (map (fun x => (ratr x.1, ratr x.2)) l).
Proof.
elim: l => [ | a l' IH].
  apply: list_R_nil_R.
by apply: list_R_cons_R.
Qed.

Lemma list_R_map2 {R : rcfType} (l : list (rat * rat)) l' :
  list_R (fun x y => ((ratr x.1, ratr x.2) : R * R) = y /\ 0 <= x.2) l l' ->
    (map (fun x => (ratr x.1, ratr x.2)) l) = l'.
Proof. 
by elim => [ // | /= [a a'] [b b'] [-> pos] lr lR] _ ->.
Qed.

Lemma list_R_map_pos {R : rcfType} (l : list (rat * rat)) l' :
  list_R (fun x y => ((ratr x.1, ratr x.2) : R * R) = y /\ 0 <= x.2) l l' ->
  all (fun x => 0 <= x.2) l'.
Proof. 
by elim => [ // | /= [a a'] [b b'] [<- pos] lr lR] _ -> /=; rewrite ler0q andbT.
Qed.

Lemma sort_den_nonzero (A : eqType) (l : seq A)
   (cmp : rel A) (pr : pred A) : all pr l ->   all pr (sort cmp l).
Proof.
by rewrite (perm_eq_all pr (perm_eqlE (perm_sort cmp l))). 
Qed.

Lemma bool_R_refl (b : bool) : bool_R b b.
 by case: b; constructor. Qed.

Hint Resolve bool_R_refl : core.

Lemma nat_R_eq x y : (nat_R x y -> x = y) * (x = y -> nat_R x y).
Proof.
elim: x y => [ | x IH] [ | y].
      by split; auto; move=> *; apply: nat_R_O_R.
    by split; try discriminate; move=> H; inversion H.
  by split; try discriminate; move=> H; inversion H.
split; last by move=> [xy]; apply: nat_R_S_R; apply: (snd (IH y)).
by move=> H; inversion H; congr (S _); apply: (fst (IH y)).
Qed.

Lemma sample_ratP (R : rcfType) (l : list (rat * rat)) :
   all (fun x => 0 <= x.2) l ->
   map (fun p => (ratr p.1, ratr p.2) : R * R) (sample_rat l) = 
   sample (map (fun p => (ratr p.1, ratr p.2)) l).
Proof.
move=> /= denpos.
apply: list_R_map2.
apply: sort_R => /=.
  move=> a b [] <- a0 a' b' [] <- b0 /=.
  rewrite orbC ler_eqVlt -quad_eqP -?quad_ltP ?ler0q // /q_eq /q_lt /quad_eq
    /quad_lt. 
  have fourq : 4%:R = ratr 1 + (ratr 1 + (ratr 1 + ratr 1)) :> R.
    by rewrite !mulrS addr0 rmorph1.
  apply: orb_R.
    by apply: (abs_quad_eq_R (Num_R := fun x (y : R) => y = ratr x)); auto;
    ((move=> x x' xx'  y y' yy'; rewrite -?[Equality.op _ _ _]/(_ == _) xx' ?yy';
     ((move/(fun a b => fst(nat_R_eq a b)): yy' => ->)||idtac)) ||
     (move=> x x' xx'; rewrite -?[Equality.op _ _ _]/(_ == _) xx') ||
     (move=> x x' xx' n; rewrite -?[Equality.op _ _ _]/(_ == _) xx') || idtac);
     rewrite ?rmorphN ?rmorphD ?rmorphM ?rmorph0 ?exprnP ?rmorphX /=
        ?(inj_eq (fmorph_inj (ratr_rmorphism R))) ?(ler_rat R) ?(ltr_rat R); auto.
  by apply: (abs_quad_lt_R (Num_R := fun x (y : R) => y = ratr x)); auto;
    ((move=> x x' xx'  y y' yy'; rewrite -?[Equality.op _ _ _]/(_ == _) xx' ?yy';
     ((move/(fun a b => fst(nat_R_eq a b)): yy' => ->)||idtac)) ||
     (move=> x x' xx'; rewrite -?[Equality.op _ _ _]/(_ == _) xx') ||
     (move=> x x' xx' n; rewrite -?[Equality.op _ _ _]/(_ == _) xx') || idtac);
     rewrite ?rmorphN ?rmorphD ?rmorphM ?rmorph0 ?exprnP ?rmorphX /=
        ?(inj_eq (fmorph_inj (ratr_rmorphism R))) ?(ler_rat R) ?(ltr_rat R); auto.
move: denpos; elim: l => [ | /= a l' IH].
  by move => _; apply: list_R_nil_R.
move/andP => [] a0 others; apply list_R_cons_R.
  by rewrite a0; split.
by apply: IH.
Qed.

